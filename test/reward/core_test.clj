(ns reward.core-test
  (:require [clojure.test :refer :all]
            [reward.core :refer :all]))

(deftest test-create-tree
  (testing "1->2"
    (is (= (create-tree [[1 2]])
           {1 [2] 2 nil})))
  (testing "1->2 1->3"
    (is (= (create-tree [[1 2] [1 3]])
           {1 [2 3] 2 nil 3 nil})))
  (testing "1->2 2->3"
    (is (= (create-tree [[1 2] [2 3]])
           {1 [2] 2 [3] 3 nil})))
  (testing "1->2 1->3 3->4 2->4"
    (is (= (create-tree [[1 2] [1 3] [3 4] [2 4]])
           {1 [2 3] 2 [] 3 [4] 4 nil})))
  (testing "1->2 1->3 3->4 2->4 4->5 4->6"
    (is (= (create-tree [[1 2] [1 3] [3 4] [2 4] [4 5] [4 6]])
           {1 [2 3] 2 [] 3 [4] 4 [5 6] 5 nil 6 nil})))
  (testing "???"
    (is (= (create-tree [[1 2] [2 3] [3 4] [4 5] [5 6] [6 7] [7 8]])
           {1 [2] 2 [3] 3 [4] 4 [5] 5 [6] 6 [7] 7 [8] 8 nil}))))

(deftest test-calculate-score-for-key
  (testing "1->2 calculate from 1"
    (is (= (calculate-score-for-key {1 [2]} [1] 0 0)
           0)))
  (testing "1->2 1->3 3->4 2->4 calculate from 1"
    (is (= (calculate-score-for-key {1 [2 3] 2 [] 3 [4]} [1] 0 0)
           2)))
  (testing "1->2 1->3 3->4 2->4 calculate from 2"
    (is (= (calculate-score-for-key {1 [2 3] 2 [] 3 [4]} [2] 0 0)
           0)))
  (testing "1->2 1->3 3->4 2->4 calculate from 3"
    (is (= (calculate-score-for-key {1 [2 3] 2 [] 3 [4]} [2] 0 0)
           0)))
  (testing "1->2 1->3 3->4 2->4 4->5 4->6 calculate from 1"
    (is (= (calculate-score-for-key {1 [2 3] 2 [] 3 [4] 4 [5 6]} [1] 0 0)
           5/2)))
  (testing "1->2 1->3 3->4 2->4 4->5 4->6 calculate from 2"
    (is (= (calculate-score-for-key {1 [2 3] 2 [] 3 [4] 4 [5 6]} [2] 0 0)
           0)))
  (testing "1->2 1->3 3->4 2->4 4->5 4->6 calculate from 3"
    (is (= (calculate-score-for-key {1 [2 3] 2 [] 3 [4] 4 [5 6]} [3] 0 0)
           1))))

(deftest test-calculate-score
  (testing "1->2"
    (is (= (calculate-score {1 [2]})
           {1 0 2 0})))
  (testing "1->2 2->3"
    (is (= (calculate-score {1 [2] 2 [3]})
           {1 1 2 0 3 0})))
  (testing "1->2 2->3"
    (is (= (calculate-score {1 [2 3] 2 [] 3 [4] 4 [5 6]})
           {1 5/2 2 0 3 1 4 0 5 0 6 0})))
  (testing "1->2 2->3"
    (is (= (calculate-score {1 [2] 2 [3] 3 [4] 4 [5] 5 [6]})
           {1 15/8 2 7/4 3 3/2 4 1 5 0 6 0}))))
