(ns reward.core
  (:gen-class)
  (:require [clojure.java.io :refer :all]
            [clojure.math.numeric-tower :as math]
            [compojure.core :refer :all]
            [compojure.route :as route]
            [ring.middleware.defaults :refer [wrap-defaults site-defaults]]
            [ring.middleware.json :refer [wrap-json-response]]
            [ring.util.response :refer [response created]]
            [ring.adapter.jetty :refer [run-jetty]]))

(def invitations (atom []))

(defn create-tree
  "Creates a tree from the inputs."
  [inputs]
  (loop [inputs inputs
         tree (sorted-map)
         invitees #{}]
    (let [input (first inputs)
          inputs-rest (rest inputs)]
      (let [customer (first input)
            invitee (last input)]
        (if (empty? input)
          tree
          (recur inputs-rest
                 (if (contains? invitees invitee)
                   (if (nil? (get tree customer))
                     (assoc tree customer [])
                     tree)
                   (conj tree {customer (vec (conj (get tree customer []) invitee)) invitee nil}))
                 (conj invitees customer invitee)))))))

(defn calculate-score-for-key
  "Calculates the score of a key in the tree."
  ([tree
    key]
   (calculate-score-for-key tree [key] 0 0))
  ([tree
    keys
    level
    score]
   (loop [keys keys
          score score]
     (let [key (first keys)
           invitees (get tree key)
           multiplier (count (filter #(some? (get tree %)) invitees))
           score (+ score (* multiplier (math/expt 1/2 level)))]
       (if (nil? key)
         score
         (if (empty? invitees)
           (recur (rest keys)
                  score)
           (calculate-score-for-key tree invitees (inc level) score)))))))

(defn calculate-score
  "Iterates over the tree and calculates the score for each key."
  [tree]
  (loop [keys (keys tree)
         score (reduce conj {} (map (fn [i] (if (nil? i) {} {i 0})) (flatten (seq tree))))]
    (let [key (first keys)
          keys-rest (rest keys)]
      (if (nil? key)
        score
        (recur keys-rest
               (assoc score key (calculate-score-for-key tree key)))))))

(defn new-invation
  "Create a new invitation into invitations atom."
  [customer invitee]
  (swap! invitations conj [customer invitee]))

(defn import-file
  "Imports a file and add the content into invitations atom."
  [path]
  (with-open [r (reader path)]
    (doseq [line (line-seq r)]
      (let [invite (map #(Integer. %) (clojure.string/split line #" "))]
        (new-invation (first invite) (last invite))))))

(defn show-scores-handler
  "Shows all the scores of the customers."
  [request]
  (response (calculate-score (create-tree @invitations))))

(defn invite-handler
  "Create a new invitation from customer to invitee."
  [request]
  (let [customer (Integer. (get-in request [:params :customer]))
        invitee (Integer. (get-in request [:params :invitee]))]
    (new-invation customer invitee)
    (created (str "/score/" customer))))

(defn show-customer-score-handler
  "Shows the score of the customer."
  [request]
  (let [customer (Integer. (get-in request [:params :customer]))
        score (calculate-score-for-key (create-tree @invitations) customer)]
    (response {customer score})))

(defn show-invites-handler
  "Shows all the current invites."
  [request]
  (response @invitations))

(defroutes app-routes
           (GET "/score" [] (wrap-json-response show-scores-handler))
           (GET "/score/:customer" [customer] (wrap-json-response show-customer-score-handler))
           (GET "/invite" [] (wrap-json-response show-invites-handler))
           (POST "/invite/:customer/:invitee" [customer invitee] (wrap-json-response invite-handler))
           (route/not-found "Not Found"))

(def app
  (wrap-defaults app-routes (assoc-in site-defaults [:security :anti-forgery] false)))

(defn -main
  [& args]
  (if (< (count args) 1)
    (do
      (println "Argument required.")
      (println "Usage: lein run input.txt"))
    (do
      (import-file (first args))
      (run-jetty app {:port 3000}))))
