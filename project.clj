(defproject reward "0.1.0-SNAPSHOT"
  :description "Reward for Customers"
  :url "http://bitbucket.org/jonatasoliveira/reward"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [org.clojure/math.numeric-tower "0.0.4"]
                 [compojure "1.4.0"]
                 [ring/ring-defaults "0.1.5"]
                 [ring/ring-jetty-adapter "1.4.0"]
                 [ring/ring-json "0.4.0"]]
  :main reward.core
  :aot [reward.core]
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}})
