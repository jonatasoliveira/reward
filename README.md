# Reward for Customers

This software collects all the invitations made by the customers
and calculates the accumulated scores. In addition it let the add
new invitations and recalculate the scores after that.

## Installation

Install lein: http://leiningen.org/.

Clone the repository or download the source code and go to the
directory of the project.

Install all the dependencies:

    $ lein deps

## Installation

To run all the tests:

    $ lein tests

## Usage

To run with lein (requires Leiningen to be installed):

    $ lein run input.txt

Running with .jar file:

    $ lein uberjar
    $ java -jar target/uberjar/reward-0.1.0-SNAPSHOT-standalone.jar input.txt

This will start a web server into port 3000 and it will available
to receive requests as defined in the API Endpoints section.

## API Endpoints

    GET http://localhost:3000/score

Shows all the scores of the input.

    GET http://localhost:3000/score/:customer

Shows the score of the customer.

    GET http://localhost:3000/invite

Shows the invitations vector.

    POST http://localhost:3000/invite/:customer/:invitee

Add a new invite to the invitations vector.
